FROM python:3.7
LABEL maintainer="Coline Carle"

RUN pip install psycopg2-binary
RUN pip install git+https://github.com/alberanid/imdbpy


VOLUME /imdb-data
ENTRYPOINT ["/usr/local/bin/s32imdbpy.py",  "/imdb-data"]
CMD ["postgres://postgres:postgres@db/postgres"]
