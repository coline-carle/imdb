1. Download dataset from https://datasets.imdbws.com/ into imdb-data folder
2. create idmb_data volume: 
	```sh
	docker volume create imdb_db
	```
3. seed with docker compose
	```sh
	docker-compose up
	```

update docker seed container with
```sh
docker-compose build
```

additional documentation about s3 and imdb.py:

https://github.com/alberanid/imdbpy/blob/master/docs/usage/s3.rst
